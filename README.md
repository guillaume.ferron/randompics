
# RandomPics

## What is RandomPics
RandomPics is a remote command processing system using audio voice

## How can I use it
Run `python3 RandomPics.py` or `RandomPics.py` or run `RandomPics` after `install.py`

## How can I implement my own
You can adapt RandomPics with theses 3 simple steps:
- Copy the file `robot.py` into a folder of your choice
- Create a CMU Sphynx dictionnary in the same folder
- Open the `robot.py` file and feed it with :
	- AUDIO (int) : 1 or 0, toggle the vocal capacity of the robot
	- ROBOTNAME : is used to name the robot command accross the system
	- DICTFILENAME : is the name of the dictionnary in the same folder

