#!/usr/bin/python3

AUDIO=0
DICTFILENAME="dict.txt"
ROBOTNAME="randomPics" #must be equals to robot_name in install.py

import time, shutil, webbrowser, asyncio, os, sys, glob
import pyaudio
from pocketsphinx import LiveSpeech, get_model_path

loop = asyncio.new_event_loop()
model_path = get_model_path()

def soundcardinformation():
    p = pyaudio.PyAudio()
    for i in range(p.get_device_count()):
            print(p.get_device_info_by_index(i))
            #otherwise you can do:
            print(p.get_default_input_device_info())

def splash():
    print("""
                  .andAHHAbnn.
               .aAHHHAAUUAAHHHAn.
              dHP^~"        "~^THb.
        .   .AHF                YHA.   .
        |  .AHHb.              .dHHA.  |
        |  HHAUAAHAbn      adAHAAUAHA  |
        I  HF~"_____        ____ ]HHH  I
       HHI HAPK""~^YUHb  dAHHHHHHHHHH IHH
       HHI HHHD> .andHH  HHUUP^~YHHHH IHH
       YUI ]HHP     "~Y  P~"     THH[ IUP
        "  `HK                   ]HH'  "
            THAn.  .d.aAAn.b.  .dHHP
            ]HHHHAAUP" ~~ "YUAAHHHH[
            `HHP^~"  .annn.  "~^YHH'
             YHb    ~" "" "~    dHF
              "YAb..abdHHbndbndAP"
               THHAAb.  .adAHHF
                "UHHHHHHHHHHU"
                  ]HHUUHHHHHH[
                .adHHb "HHHHHbn.
         ..andAAHHHHHHb.AHHHHHHHAAbnn..
    .ndAAHHHHHHUUHHHHHHHHHHUP^~"~^YUHHHAAbn.
    "~^YUHHP"   "~^YUHHUP"        "^YUP^"
    ""         "~~"
    """)

class Robot:

    def __init__(self):
        pass

    def listen(self):
        """Enter in listen mode, pass audio to sphynx, send result to answer"""
        try:
            speech = LiveSpeech(remove_noise=True,verbose=False, lm=False, kws=DICTFILENAME, kws_threshold=1e-20)
            for command in speech:
                self.answer(command.segments(detailed=False)[0])
        except Exception as E:
            print(E)
            pass

    def say(self,message):
        """print response messages and espeak it if AUDIO equals 1"""
        print(message)
        async def task():
            if AUDIO==1:
                await asyncio.sleep(1)
                os.system("espeak '"+message+"'")
        t = loop.create_task(task())
        loop.run_until_complete(t)

    def answer(self, command):
        """Simple answer"""
        self.say("I got "+command)
        key=command
        os.system("googleimagesdownload -f jpg -l 1 -k "+key)
        blob = glob.glob("downloads/"+key+'/*.jpg')
        webbrowser.open_new(blob[0])
        time.sleep(4)
        shutil.rmtree("downloads")
        command = ""

    def start():
        """Read dictionnary, instantiate and launch RandomPics"""
        with open(DICTFILENAME, 'r') as f:
            print(f.read())
            robot = Robot()
            robot.listen()
        print("exit")

def confirm_dictlocation():
    if _DICT == "global":
        print("Global dictionnary used : "+DICTFILENAME)
    else : 
        assert(_DICT == "local")
        print("local dictionnary used : "+DICTFILENAME)

if __name__ == "__main__":

    #hook to ensure that a dictfile is present
    if not os.path.isfile(DICTFILENAME):
        if not os.path.isfile("/etc/"+ROBOTNAME+"/"+DICTFILENAME):
            sys.exit("DICT not found - ABORT")
        else:
            DICTFILENAME = "/etc/"+ROBOTNAME+"/"+DICTFILENAME
            _DICT="global"
    else:
        _DICT="local"
    #end hook
    soundcardinformation()
    splash()
    confirm_dictlocation()
    Robot.start()
