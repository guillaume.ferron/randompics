
# RandomPics installation

## Install swig on your host
*debian :* sudo apt-get install swig

## Install pulseaudio on your host
*debian :* sudo apt-get install pulseaudio pulseaudio-utils libpulse-dev libpulse-java libpulse0

## Install google_images_download on your python3 implementation
*linux :* git clone https://github.com/hardikvasa/google-images-download.git
*linux :* cd google-images-download && sudo python setup.py install

## Install python dependencies (see requirements.txt for versions)
- `pip install pyaudio==version`
- `pip install pocketsphinx==version`

## run the robot stand alone
`cd ./src && ./robot.py`

## OR run the install script to integrate the robot to your system
`cd ./src && ./install.py`
