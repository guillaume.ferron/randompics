#!/usr/bin/python3

import os, sys
from robot import DICTFILENAME, ROBOTNAME

robot_name = ROBOTNAME
dictionnary_filename = DICTFILENAME

def install_app():
    """This function copy the script as /usr/bin/{robot name}"""
    if not os.path.isfile("/usr/bin/"+robot_name):
        os.system("sudo cp robot.py /usr/bin/"+robot_name)
    else:
        update = input("update application? [y|n]")
        if update == "y":
            os.system("sudo cp robot.py /usr/bin/"+robot_name)

def create_path():
    """This function create required folder in /etc"""
    if not os.path.isdir("/etc/"+robot_name):
        os.system("sudo mkdir /etc/"+robot_name)

def copy_dictionnary():
    """This function copy the dictionnary file into /etc/{required folder}"""
    if not os.path.isfile("/etc/"+robot_name+"/"+dictionnary_filename):
        os.system("sudo cp "+dictionnary_filename+" /etc/"+robot_name+"/"+dictionnary_filename)
    else:
        update = input("update dictionnary? [y|n]")
        if update == "y":
            os.system("sudo cp "+dictionnary_filename+" /etc/"+robot_name+"/"+dictionnary_filename)

def ensure_dict():
    """Check if dict file is present in the install.py and robot.py folder"""
    if not os.path.isfile(dictionnary_filename ):
        sys.exit("DICT not found - ABORT")


ensure_dict()
create_path()
copy_dictionnary()
install_app()
